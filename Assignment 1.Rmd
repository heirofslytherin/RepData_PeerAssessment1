---
title: "Reproducible Research Assignment 1"
author: "Karsten Chu"
date: "Saturday, January 17, 2015"
output: html_document
---

The following is my submission for Reproducible Research, assignment : 1.

Download, unzip and load the activity data.

```{r, echo=TRUE}
unzip("activity.zip")
data <- read.csv("activity.csv")
```
 Clean the data by removing NA values.

```{r, echo=FALSE}
cleandata <- data[!is.na(data$steps),]

```

Aggregate the data by summing all steps taken per unique date value.


```{r, echo=TRUE}
stepstotal <- aggregate(cleandata$steps~cleandata$date, cleandata, FUN = sum)
colnames(stepstotal)<- c("date", "steps")
stepsmean <- aggregate(cleandata$steps~cleandata$date, cleandata, FUN = mean)
colnames(stepsmean)<- c("date", "steps")
stepsmedian <- aggregate(cleandata$steps~cleandata$date, cleandata, FUN = median)
colnames(stepsmedian)<- c("date", "steps")

```

Histogram data.

```{r, echo=TRUE}
cat <- barplot(stepstotal$steps, main = "Histogram of Steps per Day", xlab = "Days", ylab = "# of Steps")
axis(1, at = cat, labels=stepstotal$date)
```

Aggregate the data over the intervals and plot the average number of steps per time interval.

```{r, echo=TRUE}

intervalmean <- aggregate(cleandata$steps~cleandata$interval, cleandata, FUN = mean)
colnames(intervalmean)<- c("Interval", "Avg.Steps")
plot(intervalmean, type = "l", main = "Time Series Plot of Steps per Interval", xlab = "Interval", ylab = "Avg. # of Steps")

```

Calculate the interval that contains the maximum avg. number of steps.

```{r, echo=TRUE}

intmax <- intervalmean
intmax <- intmax[(intmax$Avg.Steps == max(intmax$Avg.Steps)),]
```

Determine the number of missing values.

```{r, echo=TRUE}
#Use the number of observations in the untidy data set and subtract from it the number of rows in the tidy'd data set.
NAValues <- nrow(data)-nrow(cleandata)
```

In order to fill in the missing values, we'll replace them with their mean values.

```{r, echo=TRUE}
#Create a separate set of the "dirty" data, or the data with NA values.
dirtydata <- data[is.na(data$steps),]

#Define a function that will look up an interval and return the mean of the interval.
getIntMean <- function(interval)
  {
  match <- (intervalmean$Interval==interval)
  intervalmean[match,"Avg.Steps"]
  }

#Replace all the steps that are NA with the mean value.
dirtydata$steps <- getIntMean(dirtydata$interval)

#Merge the both cleaned sets of data.
cleantestdata <- rbind(cleandata, dirtydata)
```