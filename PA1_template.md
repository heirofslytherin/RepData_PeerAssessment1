# Reproducible Research: Peer Assessment 1


## Loading and preprocessing the data
Download, unzip and load the activity data.


```r
#Unzip and load data.
unzip("activity.zip")
data <- read.csv("activity.csv")

#Remove the NA values.
cleandata <- data[!is.na(data$steps),]
```


## What is mean total number of steps taken per day?


```r
#Calculate total steps per day.
stepstotal <- aggregate(cleandata$steps~cleandata$date, cleandata, FUN = sum)
colnames(stepstotal)<- c("date", "steps")
stepstotal
```

```
##          date steps
## 1  2012-10-02   126
## 2  2012-10-03 11352
## 3  2012-10-04 12116
## 4  2012-10-05 13294
## 5  2012-10-06 15420
## 6  2012-10-07 11015
## 7  2012-10-09 12811
## 8  2012-10-10  9900
## 9  2012-10-11 10304
## 10 2012-10-12 17382
## 11 2012-10-13 12426
## 12 2012-10-14 15098
## 13 2012-10-15 10139
## 14 2012-10-16 15084
## 15 2012-10-17 13452
## 16 2012-10-18 10056
## 17 2012-10-19 11829
## 18 2012-10-20 10395
## 19 2012-10-21  8821
## 20 2012-10-22 13460
## 21 2012-10-23  8918
## 22 2012-10-24  8355
## 23 2012-10-25  2492
## 24 2012-10-26  6778
## 25 2012-10-27 10119
## 26 2012-10-28 11458
## 27 2012-10-29  5018
## 28 2012-10-30  9819
## 29 2012-10-31 15414
## 30 2012-11-02 10600
## 31 2012-11-03 10571
## 32 2012-11-05 10439
## 33 2012-11-06  8334
## 34 2012-11-07 12883
## 35 2012-11-08  3219
## 36 2012-11-11 12608
## 37 2012-11-12 10765
## 38 2012-11-13  7336
## 39 2012-11-15    41
## 40 2012-11-16  5441
## 41 2012-11-17 14339
## 42 2012-11-18 15110
## 43 2012-11-19  8841
## 44 2012-11-20  4472
## 45 2012-11-21 12787
## 46 2012-11-22 20427
## 47 2012-11-23 21194
## 48 2012-11-24 14478
## 49 2012-11-25 11834
## 50 2012-11-26 11162
## 51 2012-11-27 13646
## 52 2012-11-28 10183
## 53 2012-11-29  7047
```

```r
#Calculate mean steps per day.
stepsmean <- aggregate(cleandata$steps~cleandata$date, cleandata, FUN = mean)
colnames(stepsmean)<- c("date", "steps")
stepsmean
```

```
##          date      steps
## 1  2012-10-02  0.4375000
## 2  2012-10-03 39.4166667
## 3  2012-10-04 42.0694444
## 4  2012-10-05 46.1597222
## 5  2012-10-06 53.5416667
## 6  2012-10-07 38.2465278
## 7  2012-10-09 44.4826389
## 8  2012-10-10 34.3750000
## 9  2012-10-11 35.7777778
## 10 2012-10-12 60.3541667
## 11 2012-10-13 43.1458333
## 12 2012-10-14 52.4236111
## 13 2012-10-15 35.2048611
## 14 2012-10-16 52.3750000
## 15 2012-10-17 46.7083333
## 16 2012-10-18 34.9166667
## 17 2012-10-19 41.0729167
## 18 2012-10-20 36.0937500
## 19 2012-10-21 30.6284722
## 20 2012-10-22 46.7361111
## 21 2012-10-23 30.9652778
## 22 2012-10-24 29.0104167
## 23 2012-10-25  8.6527778
## 24 2012-10-26 23.5347222
## 25 2012-10-27 35.1354167
## 26 2012-10-28 39.7847222
## 27 2012-10-29 17.4236111
## 28 2012-10-30 34.0937500
## 29 2012-10-31 53.5208333
## 30 2012-11-02 36.8055556
## 31 2012-11-03 36.7048611
## 32 2012-11-05 36.2465278
## 33 2012-11-06 28.9375000
## 34 2012-11-07 44.7326389
## 35 2012-11-08 11.1770833
## 36 2012-11-11 43.7777778
## 37 2012-11-12 37.3784722
## 38 2012-11-13 25.4722222
## 39 2012-11-15  0.1423611
## 40 2012-11-16 18.8923611
## 41 2012-11-17 49.7881944
## 42 2012-11-18 52.4652778
## 43 2012-11-19 30.6979167
## 44 2012-11-20 15.5277778
## 45 2012-11-21 44.3993056
## 46 2012-11-22 70.9270833
## 47 2012-11-23 73.5902778
## 48 2012-11-24 50.2708333
## 49 2012-11-25 41.0902778
## 50 2012-11-26 38.7569444
## 51 2012-11-27 47.3819444
## 52 2012-11-28 35.3576389
## 53 2012-11-29 24.4687500
```

```r
#Calculate median steps per day. (Seems broken, but it seems that the # of zeros for steps in each data set > 1/2 # obs, so it might make sense.).
stepsmedian <- aggregate(cleandata$steps~cleandata$date, cleandata, FUN = median)
colnames(stepsmedian)<- c("date", "steps")
stepsmedian
```

```
##          date steps
## 1  2012-10-02     0
## 2  2012-10-03     0
## 3  2012-10-04     0
## 4  2012-10-05     0
## 5  2012-10-06     0
## 6  2012-10-07     0
## 7  2012-10-09     0
## 8  2012-10-10     0
## 9  2012-10-11     0
## 10 2012-10-12     0
## 11 2012-10-13     0
## 12 2012-10-14     0
## 13 2012-10-15     0
## 14 2012-10-16     0
## 15 2012-10-17     0
## 16 2012-10-18     0
## 17 2012-10-19     0
## 18 2012-10-20     0
## 19 2012-10-21     0
## 20 2012-10-22     0
## 21 2012-10-23     0
## 22 2012-10-24     0
## 23 2012-10-25     0
## 24 2012-10-26     0
## 25 2012-10-27     0
## 26 2012-10-28     0
## 27 2012-10-29     0
## 28 2012-10-30     0
## 29 2012-10-31     0
## 30 2012-11-02     0
## 31 2012-11-03     0
## 32 2012-11-05     0
## 33 2012-11-06     0
## 34 2012-11-07     0
## 35 2012-11-08     0
## 36 2012-11-11     0
## 37 2012-11-12     0
## 38 2012-11-13     0
## 39 2012-11-15     0
## 40 2012-11-16     0
## 41 2012-11-17     0
## 42 2012-11-18     0
## 43 2012-11-19     0
## 44 2012-11-20     0
## 45 2012-11-21     0
## 46 2012-11-22     0
## 47 2012-11-23     0
## 48 2012-11-24     0
## 49 2012-11-25     0
## 50 2012-11-26     0
## 51 2012-11-27     0
## 52 2012-11-28     0
## 53 2012-11-29     0
```

## What is the average daily activity pattern?


```r
#Plot histogram/barplot for steps per day.
cat <- barplot(stepstotal$steps, main = "Histogram of Steps per Day", xlab = "Days", ylab = "# of Steps")
#Add axis, labeling dates on x-axis.
axis(1, at = cat, labels=stepstotal$date)
```

![](PA1_template_files/figure-html/unnamed-chunk-3-1.png) 

Aggregate the data over the intervals and plot the average number of steps per time interval.


```r
intervalmean <- aggregate(cleandata$steps~cleandata$interval, cleandata, FUN = mean)
colnames(intervalmean)<- c("Interval", "Avg.Steps")
plot(intervalmean, type = "l", main = "Time Series Plot of Steps per Interval", xlab = "Interval", ylab = "Avg. # of Steps")
```

![](PA1_template_files/figure-html/unnamed-chunk-4-1.png) 

Calculate the interval that contains the maximum avg. number of steps.


```r
intmax <- intervalmean
intmax <- intmax[(intmax$Avg.Steps == max(intmax$Avg.Steps)),]
intmax
```

```
##     Interval Avg.Steps
## 104      835  206.1698
```
```

## Imputing missing values

Our strategy for inputting missing values will be to replace NA values with the mean value of steps for that specific interval.  

```r
#Use the number of observations in the untidy data set and subtract from it the number of rows in the tidy'd data set.
NAValues <- nrow(data)-nrow(cleandata)

#Create a separate set of the "dirty" data, or the data with NA values.
dirtydata <- data[is.na(data$steps),]

#Define a function that will look up an interval and return the mean of the interval.
getIntMean <- function(interval)
  {
  match <- (intervalmean$Interval==interval)
  intervalmean[match,"Avg.Steps"]
  }

#Replace all the steps that are NA with the mean value.
dirtydata$steps <- getIntMean(dirtydata$interval)

#Merge the both cleaned sets of data.
cleantestdata <- rbind(cleandata, dirtydata)

#Calculate total steps per day.
stepstotal <- aggregate(cleantestdata$steps~cleantestdata$date, cleantestdata, FUN = sum)
colnames(stepstotal)<- c("date", "steps")
#Calculate mean steps per day.
stepsmean <- aggregate(cleantestdata$steps~cleantestdata$date, cleantestdata, FUN = mean)
colnames(stepsmean)<- c("date", "steps")
#Calculate median steps per day. (Broken).
stepsmedian <- aggregate(cleantestdata$steps~cleantestdata$date, cleantestdata, FUN = median)
colnames(stepsmedian)<- c("date", "steps")
```

```r
#Plot new histogram/barplot for steps per day after missing values.
cat <- barplot(stepstotal$steps, main = "Histogram of Steps per Day (NA removed)", xlab = "Days", ylab = "# of Steps")
#Add axis, labeling dates on x-axis.
axis(1, at = cat, labels=stepstotal$date)
```

![](PA1_template_files/figure-html/unnamed-chunk-7-1.png) 


## Are there differences in activity patterns between weekdays and weekends?


```r
#Convert weekdays to "weekend"/"weekday"
convertDay <- function(day)
  {
  if ((day == "Saturday")|(day == "Sunday"))
    "Weekend"
  else 
    "Weekday"
  
  }

#Add weekday factor to dataset.
#cleantestdata$day = weekdays(as.Date(cleantestdata$date))
#cleantestdata$weekday = sapply(cleantestdata$day, convertDay(cleantestdata$day))
#cleantestdata$weekday <- as.factor(cleantestdata$weekday)
#cleantestdata$day <- as.factor(cleantestdata$day)
#head(cleantestdata)
#str(cleantestdata)

#Aggregate step means by weekend/weekday.
```
